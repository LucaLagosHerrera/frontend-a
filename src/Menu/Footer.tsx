import styled from "styled-components";
import Checkbox from "./CheckBox";
import acceptTwo from "../images/acceptTwo.png";
import reset from "../images/reset.png";
import cancel from "../images/cancel.png";
import { useEffect, useState } from "react";

const ContainerOne = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const ContainerTwo = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
  padding-top: 25px;
  padding-bottom: 25px;
  border-top: 1px solid #ffffff80;
`;

interface option {
  icon: any;
  onClick: () => void;
}

export interface params{
  enableReset: boolean;
  enableCancel: boolean;
}

const App = (params: params) =>	 {

  /*const [options, setOptions] = useState<options[]>([
    {icon: accept, onClick: () => {}}
  ]);

  useEffect(() => {
  }, [])*/

  const handleAccept = () => {
    if (typeof params.onAccept === "function") params.onAccept();
  };
  const handleReset = () => {
    if (typeof params.onReset === "function") params.onReset();
  };
  const handleCancel = () => {
    if (typeof params.onCancel === "function") params.onCancel();
  };

  /*switch (version) {

    case 1:
      let dataOne = [
        { icon: accept, onClick: handleAccept },
        { icon: cancel, onClick: handleCancel },
      ];
      return <ContainerOne></ContainerOne>;
    case 2:
      let dataTwo = [
        { icon: accept, onClick: handleAccept },
        { icon: reset, onClick: handleReset },
        { icon: cancel, onClick: handleCancel },
      ];
      return <ContainerTwo></ContainerTwo>;
    default:
      return <></>;
  }*/
  
  const data: option[] = [
    { icon: acceptTwo, onClick: handleAccept },
    { icon: reset, onClick: handleReset },
    { icon: cancel, onClick: handleCancel },
  ];
  <ContainerOne>
    {data.map((value, index) => <Checkbox image={value.icon} onClick={value.onClick}/>)}
  </ContainerOne>
};

export default App;
